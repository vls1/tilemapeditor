﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TileMapEditor
{
    public partial class MainWindow : Window
    {
        Loader loader = new Loader(@"C:\Levels.txt");
        public MainWindow()
        {
            InitializeComponent();
            Loaded += LoadLevels;
        }

        private void LoadLevels(object sender, RoutedEventArgs e) 
        {
            loader.LoadLevels();
        }
    }
}
