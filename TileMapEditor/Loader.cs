﻿using System.Collections.Generic;
using System.IO;

namespace TileMapEditor
{
    public enum Tile 
    { 
        none,
        player,
        wall
    }

    public class Loader
    {
        public List<Tile[,]> levelsCollection;
        public string filePath;

        public Loader(string filePath) 
        {
            levelsCollection = new List<Tile[,]>();
            this.filePath = filePath;
        }

        public void LoadLevels() 
        {
            string[] content = null;

            int width = 13;
            int height = 13;

            Tile[,] level = new Tile[width, height];

            try
            {
                content = File.ReadAllLines(filePath);
            }
            catch 
            {
                return;
            }

            int currLine = 0;

            while (currLine < content.Length) 
            {
                for (int i = 0; i < width; i++) 
                {
                    for (int j = 0; j < height; j++) 
                    { 
                        level[i,j] = CharToTile(content[currLine][j]);
                    }
                    currLine++;
                }
                levelsCollection.Add(level);
            }
        }

        public Tile CharToTile(char tile) 
        {
            switch (tile) 
            {
                case ' ' : return Tile.none;
                case '#' : return Tile.wall;
                case '@' : return Tile.player;
                default  : return Tile.none;
            }
        }
    }

    
}
